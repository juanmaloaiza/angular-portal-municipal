export class UsuarioModel {

  constructor(
    public nombre: string,
    public email: string,
    public password?: string,
    public passwordConfirmation?:string,
    public role?: string,
    public uid?: string)
  {

  }
}
