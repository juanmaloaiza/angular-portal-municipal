import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

declare const gapi:any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  public formSummitted=false;
  public auth2: any;

  public loginForm = this.fb.group({
    email:[localStorage.getItem('email') || '', [Validators.required, Validators.email]],
    password:['', [Validators.required, Validators.minLength(4)]],
    rememberMe:[false]
  });

  constructor(private router:Router,
              private fb: FormBuilder,
              private usuarioService: UsuarioService) { }



  ngOnInit(): void {
    this.renderButton();
  }

  login ()
  {
    console.log(this.loginForm.value);
    this.usuarioService.login(this.loginForm.value)
    .subscribe( resp => {
      if(this.loginForm.get('rememberMe').value)
      {
        localStorage.setItem('email', this.loginForm.get('email').value);
      }
      else
      {
        localStorage.removeItem('email');
      }
      //Navegar al Dashboard
      this.router.navigateByUrl('/');
    }, err => {
      Swal.fire('Error', err.error.error.message, 'error');
    });

  }

  onSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    var id_token = googleUser.getAuthResponse().id_token;
    console.log(id_token);
  }
  onFailure(error) {
    console.log(error);
  }

  renderButton() {
    gapi.signin2.render('my-signin2', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': this.onSuccess,
      'onfailure': this.onFailure
    });

    this.startApp ();
  }


  startApp () {
    gapi.load('auth2', () =>{
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      this.auth2 = gapi.auth2.init({
        client_id: '356487568309-rf0ipss8b8dlkvjtne2jrn6gcvghnneo.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
      });
      this.attachSignin(document.getElementById('my-signin2'));
    });
  };

  attachSignin(element) {
    console.log(element.id);
    this.auth2.attachClickHandler(element, {},
        (googleUser: any) => {
          var id_token = googleUser.getAuthResponse().id_token;
          // TODO: mover al dashboard y mejorar el login de googleUser
          this.usuarioService.loginGoogleAccount(id_token);
          //Navegar al Dashboard
          this.router.navigateByUrl('/');
        }, ( error: any) =>{
          alert(JSON.stringify(error, undefined, 2));
        });
  }

}
