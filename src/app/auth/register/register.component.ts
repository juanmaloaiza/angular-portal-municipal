import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit {

  public formSummitted=false;

   registerForm = this.fb.group({
    nombre:['', [Validators.required, Validators.minLength(3)]],
    email:['', [Validators.required, Validators.email]],
    password:['1234', [Validators.required, Validators.minLength(4)]],
    passwordConfirmation:['1234', [Validators.required, Validators.minLength(4)]],
    terminos:[false, Validators.required],
  },{
    validators:this.validarPassword('password', 'passwordConfirmation'),
  });

  constructor(private fb: FormBuilder, private usuarioService: UsuarioService) { }

  crearUsuario (){
      this.formSummitted=true;
      console.log(this.registerForm.value);
      if (this.registerForm.invalid)
      {return; }

      //realizar la creacion.
      this.usuarioService.createUser(this.registerForm.value)
      .subscribe( resp => {
        console.log('usuario creado');
        console.log(resp);
      }, err => { console.warn(err.error.error.message)
        Swal.fire('Error', err.error.error.message, 'error');
      }

      );




  }

  campoNoValido (campo: string): boolean {
    if (this.registerForm.get(campo).invalid && this.formSummitted){
      return true;
    }
    else
    {return false;}
  }

  aceptaTerminos(){
    return !this.registerForm.get('terminos').value && this.formSummitted;
  }

  passwordNoValido ()
  {
    const pass1= this.registerForm.get('password').value;
    const pass2= this.registerForm.get('passwordConfirmation').value;

    if ((pass1 !== pass2) && this.formSummitted)
    {
      return true;
    }
    else
    {return false}
  }

  validarPassword(pass1Name: string, pass2Name: string)
  {
    return (formGroup: FormGroup) => {
      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value=== pass2Control.value)
      {
          pass2Control.setErrors(null)
      }
      else
      {
        pass2Control.setErrors({noEsIgual: true});
      }

    }
  }


  ngOnInit(): void {


  }

}
