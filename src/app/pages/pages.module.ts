import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ConsultaImpuestoComponent } from './consulta-impuesto/consulta-impuesto.component';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { AcountSettingsComponent } from './acount-settings/acount-settings.component';


@NgModule({
  declarations: [
    ConsultaImpuestoComponent,
    PagesComponent,
    DashboardComponent,
    AcountSettingsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
  ],
  exports: [
    ConsultaImpuestoComponent,
    PagesComponent,
    DashboardComponent,
    AcountSettingsComponent
  ]
})
export class PagesModule { }
