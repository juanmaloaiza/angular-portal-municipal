import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConsultaImpuestoComponent } from './consulta-impuesto/consulta-impuesto.component';
import { AcountSettingsComponent } from './acount-settings/acount-settings.component';


const routes: Routes =[
  {
    path: '',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', component: DashboardComponent, data:{titulo:'Tablero'}},
      {path: 'consultas', component:ConsultaImpuestoComponent, data:{titulo:'Consultas'}},
      {path: '', redirectTo:'/dashboard', pathMatch:'full'},
      {path: 'account-settings', component:AcountSettingsComponent, data:{titulo:'Configuracion'}},
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
