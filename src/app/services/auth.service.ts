import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UsuarioModel } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url ='https://identitytoolkit.googleapis.com/v1/accounts:';
  private API_KEY='AIzaSyCKUQJdxBhYtW0_lndGm7_BX4gx2GUXuCM';



  //Crear nuevo usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  //Login de Usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  constructor( private http: HttpClient,) { }

  createUser (usuario: UsuarioModel) {
      const autData = {
        email: usuario.email,
        password: usuario.password,
        returnSecureToken: true
      };

      return this.http.post(
      `}${ this.url }/signUp?key=${ this.API_KEY}`, autData);
  }

  login (usuario: UsuarioModel) {

  }
  logout () {}

}
