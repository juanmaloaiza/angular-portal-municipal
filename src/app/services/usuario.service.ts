import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { RegisterForm } from '../interfaces/register-form.interface';
import { environment } from '../../environments/environment';

const baseUrl = environment.base_url;
const apiKey = environment.api_key;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor( private http: HttpClient) { }


  //Crear nuevo usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]
  createUser (formData: RegisterForm) {

    const autData = {
      email: formData.email,
      password: formData.password,
      returnSecureToken: true
    };
    return this.http.post(
      `${ baseUrl }signUp?key=${ apiKey}`, autData)
      .pipe(
        tap( (resp:any) => {
          localStorage.setItem('token',resp.idToken);
        })
      );

  }

    //Login de Usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]
  login (formData: RegisterForm)
  {
    console.log ('Login usuario');
    const autData = {
      email: formData.email,
      password: formData.password,
      returnSecureToken: true
    };
    return this.http.post(
      `${ baseUrl }signInWithPassword?key=${ apiKey}`, autData)
      .pipe(
        tap( (resp:any) => {
          localStorage.setItem('token',resp.idToken);
        })
      );

  }

  loginGoogleAccount (token: any) {

    localStorage.setItem('token',token);

  }

  validarToken(): boolean {

    const token = localStorage.getItem('token') || '';
    console.log (token);
    if (token.length >2) {
      return true;
    }
    else
      return true;
  }
}
